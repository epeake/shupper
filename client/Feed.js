/*
 * All posts of connections and self
 */
import React, { Component } from "react";
import { View , Text, TouchableOpacity, ScrollView, StyleSheet} from "react-native";
import {Header, Backdrop} from "./General.js"


/*
 * Represents a single post object of our feed
 */
const Post = (props) => {
  return (
    <View style={styles.post}>
        <Text style={styles.post_num}>{props.num}</Text>
        <Text style={styles.post_text}>{props.text}</Text>
    </View>
  );
};


export default class Feed extends Component {
  constructor(props){
    super(props);

    this.state = {
      posts_: undefined   // list of all posts
    }
    this.controller = new AbortController(); // used to cancel in-progress fetch on component unmount

    this.getAllPosts = this.getAllPosts.bind(this);
  }
  

  /*
   * just calls getAllPosts on startup
   */
  componentDidMount() {
    this.getAllPosts();
  }


  componentWillUnmount() {
    this.controller.abort();
  }

  /*
   * populates the posts_ with our entire initial feed
   */
  async getAllPosts() {
    try {
      if (this.props.token !== undefined) {
        const res = await fetch("https://epeake.dev/post/feed"
          , {method: "GET", signal: this.controller.signal, headers: {"Authorization": "Bearer " + this.props.token,}}
        );
        let res_json = await res.json();

        // newest post up top
        res_json = res_json.sort((a, b) => parseInt(a.p_id) < parseInt(b.p_id));
        this.setState({posts_: res_json});
      }
    } catch (e) {
      console.log(e);
    }
  }


  render() {
    let posts = []
    if (this.state.posts_ !== undefined){
      posts = this.state.posts_.map((obj, i) => {
        return (
          
          // need to convert numbers from countrycode+num to +countrycodenum format (+11234567890)
          <Post text={obj.post} num={`+${obj.num.replace('+', '')}`} key={`post ${i}`}/>
        );
      });
    }

    return (
      <Backdrop>
        <Header/>

        <ScrollView style={{flex : 1}}>
          <View style={{padLeft: 120}}>
            {posts}
          </View>
        </ScrollView>

        <View style={{flexDirection: "row"}}>
          <TouchableOpacity
            activeOpacity={.75}
            onPress={this.props.goToPost}
            style={styles.touchable_opacity}
          >
                <Text style={styles.touchable_opacity_text}>Post</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={.75}
            onPress={this.props.goToConnect}
            style={styles.touchable_opacity}
          >
                <Text style={styles.touchable_opacity_text}>Connect</Text>
          </TouchableOpacity>
          </View>
      </Backdrop>
      );
    }
  }


const styles = StyleSheet.create({
  post_num: {
    fontWeight: "100",
    fontSize: 30,
    color: "#FFEBCD",
  }, 
  post_text: {
    fontWeight: "100",
    fontSize: 20,
    paddingTop: 15,
    paddingLeft: 20,
    paddingRight: 20,
    color: "#FFEBCD",
  },
  post: {
    paddingTop: 15,
    paddingBottom: 15, 
    alignItems: "center",
    borderColor: "white",
    backgroundColor: "#555",
    borderWidth: .2
  },
  touchable_opacity: {
    backgroundColor: "#336",
    flex: 1,
    height: 100,
    justifyContent: "center",
    borderRightWidth: 1,
  },
  touchable_opacity_text: {
    color: "#FFEBCD",
    fontSize: 30,
    fontWeight: "300",
    textAlign: "center",
  },
});
