/*
 * File contains general components reused across several pages
 */
import React from "react";
import { KeyboardAvoidingView, TouchableOpacity, View, Text, StyleSheet} from "react-native";
import Constants from "expo-constants";

const GO_BACK_PAD = 80;
const GO_BACK_WIDTH = 50;

/*
 * blank blackground that fills the screen with correct color
 */
const Backdrop = (props) => {
  if (Platform.OS == "ios") {
    return (
      <KeyboardAvoidingView
      behavior={"padding"}
      style={{flex: 1, backgroundColor: "#444"}}>
        {props.children}
      </KeyboardAvoidingView>
    );
  } else {
    return (
      <View style={{flex: 1, backgroundColor: "#444"}}>
        {props.children}
      </View>
    ); 
  }
  
};


/*
 * the "INK" header
 */
const Header = (props) => {
  return (
    <>
      <View style={styles.blank_bar}/>

      <View style={styles.header}>
            <Text style={styles.header_text}> INK </Text>
      </View>
    </>
  );
};

/*
 * the "INK" header
 */
const HeaderGoBack = (props) => {
  return (
    <>
      <View style={styles.blank_bar}/>

      <View style={styles.header_go_back}>
        <TouchableOpacity
            activeOpacity={.75}
            onPress={props.onPress}
            style={styles.go_back_opacity}
          >
            <Text style={styles.go_back_opacity_text}> {"<"}</Text>
        </TouchableOpacity>

        {/*whitespace filler*/}
        <View style={{width: GO_BACK_PAD}}></View> 
        <Text style={styles.header_go_back_text}> INK </Text>
      </View>
    </>
  );
};


/*
 * the "INK" header, but big
 */
const BigHeader = (props) => {
  return (
    <View style={styles.header_big}>
      <Text style={styles.header_big_text}> INK </Text>
    </View>
  );
};

const Loading = (props) => {
  return (
    <Backdrop>
      <View style={styles.header_big}>
        <Text style={styles.header_text}>Loading</Text>
      </View>
    </Backdrop>
  );
}

const styles = StyleSheet.create({
  blank_bar: {
    height: Constants.statusBarHeight,
    backgroundColor: "#444",
  },
  header: {
    height: 80,
    lineHeight: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#444",
    paddingBottom: 30,
  },
  header_text: {
    fontWeight: "100",
    fontSize: 50,
    color: "#FFEBCD",
  },
  header_go_back: {
    height: 80,
    lineHeight: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#444",
    paddingBottom: 30,
    flexDirection: "row",
    paddingRight: GO_BACK_WIDTH + GO_BACK_PAD,
  },
  header_go_back_text: {
    fontWeight: "100",
    fontSize: 50,
    color: "#FFEBCD",
  },
  header_big: {
    flex: .5,
    alignItems: "center",
    justifyContent: "center",
  },
  header_big_text: {
    fontWeight: "100",
    fontSize: 70,
    color: "#FFEBCD",
  },
  go_back_opacity: {
    backgroundColor: "#336",
    width: GO_BACK_WIDTH,
    height: GO_BACK_WIDTH,
    borderRadius: 10,
    justifyContent: "center",
    paddingRight: 5,
  },
  go_back_opacity_text: {
    color: "#FFEBCD",
    fontSize: 30,
    fontWeight: "300",
    textAlign: "center",
  },
});


module.exports = {
  Backdrop,
  Header,
  HeaderGoBack,
  BigHeader,
  Loading,
};

