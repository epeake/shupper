/*
 * Screen to checking whether the sent verification code is valid for sign-up
 */
import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import {Backdrop, BigHeader} from "./General.js"


export default class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code_: ''    // verification code sent to the user
    };
  }


  /*
   * send the verificaiton code to the user's provided phone number
   */
  componentDidMount() {
    this.props.getCode();
  }


  render() {
    return (
      <Backdrop>

        <BigHeader/>

        <View style={styles.input}>
          <TextInput 
            placeholder={'enter verification code'} 
            placeholderTextColor={'#666'}
            keyboardType={'number-pad'}
            style={styles.num}
            maxLength={6}
            value={this.state.code_}
            onChangeText={(number) => this.setState({code_: number})}
          />
        </View>

        {/* outer container prevents shifting from conditional button displaying */}
        <View style={styles.button}> 

        {/* only display the button if we have enough letters */}
        {
          this.state.code_.length === 6 &&
          <TouchableOpacity
            activeOpacity={.75}
            onPress={() => this.props.verify(this.state.code_)}
            style={styles.touchable_opacity}
          >
            <Text style={styles.touchable_opacity_text}>verify</Text>
          </TouchableOpacity>
        }
        </View>
        
      </Backdrop>
    );
  }
}



const styles = StyleSheet.create({
  welcome: {
    flex: .5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome_text: {
    lineHeight: 110,
    fontWeight: '100',
    fontSize: 70,
    color: '#FFEBCD',
  },
  input: {
    height: 70,
    flexDirection:"row",
  },
  num: {
    flex: 1,
    fontSize: 35,
    textAlign: 'center',
    color: '#FFEBCD',
    fontWeight: '300',
    backgroundColor: '#333'
  },
  button: {
    flex: .5,
  },
  touchable_opacity: {
    backgroundColor: "#336",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  touchable_opacity_text: {
    textAlign: "center",
    color: "#FFEBCD",
    fontSize: 35,
    lineHeight: 45,
    fontWeight: "300",
    width: 1000,
  },
});
