/*
 * Screen where you enter your number to revalidate your number
 */
import React, { Component } from "react";
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from "react-native";
import {Backdrop, BigHeader} from "./General.js"

export default class Login extends Component {
  constructor(props){
    super(props);

    this.state = {
      extension_: '1',   // user's input country code
      number_: ''        // user's input cell number
    };

    this.displayExtension = this.displayExtension.bind(this);
    this.changeExtension = this.changeExtension.bind(this);
    this.changeNumber = this.changeNumber.bind(this);             
    this.handlePress = this.handlePress.bind(this);
  }


  /*
   * properly format the extension
   */
  displayExtension() {
    return (this.state.extension_.length > 0) ? `+${this.state.extension_}`: '';
  }


  /*
   * change extension
   */
  changeExtension(extension) {
    this.setState({extension_: extension.replace('+', '')});
  }


  /*
   * change number
   */
  changeNumber(number) {
    this.setState({number_: number})
  }


  /*
   * sets number state and changes view to the authorization page
   */
  handlePress() {
    
    // TODO: need to validate the number before setting state
    const number = `${this.state.extension_}+${this.state.number_}`;
    this.props.goToAuth(number);
  }


  render() {
    return (
      <Backdrop>

        <BigHeader/>

         {/* our extension and number inputs */}
        <View style={styles.input}>
          <TextInput 
            placeholder={'+'}
            placeholderTextColor={"#666"}
            keyboardType={"number-pad"}
            style={styles.ext} 
            maxLength={4}
            value={this.displayExtension()}
            onChangeText={this.changeExtension}
          />
          <TextInput 
            placeholder={"cellphone number"} 
            placeholderTextColor={"#666"}
            keyboardType={"number-pad"}
            style={styles.num}
            maxLength={10}
            value={this.state.number_}
            onChangeText={this.changeNumber}
          />
        </View>

        
        {/* outer container prevents shifting from conditional button displaying */}
        <View style={styles.button}> 

        {/* only display the button if we have enough letters */}
        {
          this.state.number_.length === 10 && this.state.extension_.length > 0 &&
          <TouchableOpacity
            activeOpacity={.75}
            onPress={this.handlePress}
            style={styles.touchable_opacity}
          >
            <Text style={styles.touchable_opacity_text}>send</Text>
          </TouchableOpacity>
        }
        </View>
      </Backdrop>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: .5,
    alignItems: "center",
    justifyContent: "center",
  },
  header_text: {
    fontWeight: "100",
    fontSize: 70,
    color: "#FFEBCD",
  },
  input: {
    height: 70,
    flexDirection:"row",
  },
  ext: {
    flex: .3,
    fontSize: 28,
    textAlign: "right",
    paddingRight: 10,
    color: "#FFEBCD",
    backgroundColor: "#555"
  },
  num: {
    flex: .70,
    fontSize: 30,
    textAlign: "left",
    paddingLeft: 10,
    color: "#FFEBCD",
    backgroundColor: "#333"
  },
  button: {
    flex: .5,
  },
  touchable_opacity: {
    backgroundColor: "#336",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  touchable_opacity_text: {
    textAlign: "center",
    color: "#FFEBCD",
    fontSize: 35,
    lineHeight: 45,
    fontWeight: "300",
    width: 1000,
  },
});
