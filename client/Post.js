/*
 * Screen to create a new post to be sent to yours and others" feeds
 */
import React, { Component } from "react";
import {Text, TouchableOpacity, StyleSheet, TextInput, View} from "react-native";
import {HeaderGoBack, Backdrop} from "./General.js"
import base64 from "react-native-base64";


export default class Feed extends Component {
  constructor(props){
    super(props);

    this.state = {
      post_: ""     // post text
    };

    this.changePost = this.changePost.bind(this);
    this.post = this.post.bind(this);
  }

  
  /*
   * update the saved text
   */
  changePost(post) {
    this.setState({post_: post})
  }


  /*
   * post the saved post_ text to our database
   */
  async post() {
    try {
      if (this.state.post_.length === 0) {
        alert("nothing to post");
      }
      else if (this.props.token !== undefined) {
        const post_res = await fetch(`https://epeake.dev/post/${base64.encode(this.state.post_)}`
          , {method: "POST", headers: {"Authorization": "Bearer " + this.props.token,}}
        );
        const post_json = await post_res.json();

        //console.log(post_json);
        if (post_json.success) {
          this.props.goToFeed();
        } else if (!post_json.logged_in) {
          this.props.goToLogin();
        } else {
          alert("error while posting");
        }
      } else {
        this.props.goToLogin();
      }    
    } catch (e) {
      console.log(e);
      alert("error while posting");
    }
  }


  render() {
    return (
      <Backdrop>
        <HeaderGoBack onPress={this.props.goToFeed} />

        <View style={{flex: 1}}>
          <TextInput 
            placeholder={"enter post"}
            placeholderTextColor={"#666"}
            maxLength={250}
            onChangeText={this.changePost}
            style={styles.post_text}
            multiline={true}
          />
        </View>

        <View style={{flexDirection: "row"}}>
          <TouchableOpacity
            activeOpacity={.75}
            onPress={this.post}
            style={styles.touchable_opacity}
          >
            <Text style={styles.touchable_opacity_text}>Post</Text>
          </TouchableOpacity>
        </View>
      </Backdrop>
    );
  }
}


const styles = StyleSheet.create({
  post_text: {
    fontSize: 30,
    flexWrap: "wrap",
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    color: "#FFEBCD",
  },
  touchable_opacity: {
    backgroundColor: "#336",
    flex: 1,
    height: 100,
    justifyContent: "center",
  },
  touchable_opacity_text: {
    color: "#FFEBCD",
    fontSize: 30,
    fontWeight: "300",
    textAlign: "center",
  },
});
