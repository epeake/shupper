import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, AppState } from 'react-native';
import * as Location from 'expo-location';
import {Loading, Backdrop, HeaderGoBack} from "./General.js"

/*
 * the connection page
 */
export default class Conn extends Component {
  constructor(props){
    super(props);
    this.state = {
                  screen_: 'start',
                  appState_: AppState.currentState, // used to figure out whether the app has been minimized
                  enteredCode_: undefined,
                  initiatorCode_: undefined,
                  connectionUsername_: undefined,
                  myCode_: undefined,
                  };

    this.webSocket;

    this.getPairingCode = this.getPairingCode.bind(this);
    this.sendPairingRequest = this.sendPairingRequest.bind(this);
    this.sendPairingResponse = this.sendPairingResponse.bind(this);
    this.loadPairingScreen = this.loadPairingScreen.bind(this);
    this.updateLocation = this.updateLocation.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  /*
   * start the app state listener when mounted
   */
  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  /*
   * close the websocket and remove any event listeners when unmounted
   */
  componentWillUnmount() {
    if(this.webSocket){
      this.webSocket.removeEventListener('close', this.handleClose);
      this.webSocket.close();
    }
    AppState.removeEventListener('change', this.handleAppStateChange);
    this.setState({screen_: 'start'});
  }

  /*
   * update the component state when the app state changes
   */
  handleAppStateChange = nextAppState => {
    if (nextAppState.match(/inactive|background/) && this.state.appState_ === 'active') {
      if(this.webSocket){
        this.webSocket.close();
      }
      this.setState({screen_: 'start', appState_: nextAppState });
    }else{
      this.setState({ appState_: nextAppState });
    }
  };

  /*
   * handle websocket close event
   */
  handleClose(){
    this.setState({screen_: 'start'});
  }

  /*
   * update the location stored on the server
   */
  updateLocation(){
    this.setState({screen_: 'load'});
    this.getPermission();
  }

  /*
   * get location permission then get location
   */
  getPermission(){
    Location.getPermissionsAsync()
      .then((locData) => {
        if(locData.granted){
          this.getLocation();
        }else{
          Location.requestPermissionsAsync()
            .then((response) => {
              if(response.granted){
                this.getLocation();
              }else{
                console.log("Couldn't get location permision");
              }
            }).catch((error) => {
              console.log(error);
            })
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  /*
   * get location and send it to the server
   */
  getLocation() {
    Location.getCurrentPositionAsync({accuracy: 6})
      .then((location) => {
        if(location.coords.accuracy <= 16){ // hardcoded to 16
          this.webSocket.send(JSON.stringify({type: 'location', location: location}));
        }else{
          this.getPermission();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  /*
   * get a new pairing code from the server
   */
  getPairingCode(){
    if (this.webSocket && this.webSocket.readyState === this.webSocket.OPEN){
      this.webSocket.send(JSON.stringify({type: 'newCode'}));
    } else{
      this.connectSocket();
    }
  }

  /*
   * connect to the server
   */
  connectSocket() { 
    this.webSocket = new WebSocket('wss://epeake.dev/connect/');
    this.webSocket.onopen = (event) => { // event not used 
      this.webSocket.send(JSON.stringify({type: 'auth', token: this.props.token}));
      
      this.webSocket.onmessage = (event) => {
        var msg = JSON.parse(event.data);
        
        switch(msg.type) {
          case "auth":
            this.getPairingCode();
            break;
          case "request":
            this.setState({initiatorCode_: msg.initiatorCode, screen_: 'respond'});
            break;
          case "success":
            this.setState({screen_: 'success', connectionUsername_: msg.username});
            break;
          case "newCode":
            this.receivePairingCode(msg.newCode);
            break;
          case "location":
            this.setState({screen_: 'pair'})
            break;
          case "error":
            this.setState({screen_: 'error'});
            break;
          case "timeout":
            this.setState({screen_: 'timeout'});
            break;
        }
      };
      this.webSocket.addEventListener('close', this.handleClose);
    };
  }

  /*
   * request to pair with another user
   */
  sendPairingRequest(receiverCode){
    this.webSocket.send(JSON.stringify({type: 'request', receiverCode: receiverCode}));
    this.setState({screen_: 'pending', enteredCode_: undefined});
  }

  /*
   * respond to a pairing request
   */
  sendPairingResponse(response){
    this.webSocket.send(JSON.stringify({type: 'response', response: response, initiatorCode: this.state.initiatorCode_}));
    if(response === 'accept'){
      this.setState({screen_: 'load'});
    }else{
      this.webSocket.close();
      this.setState({screen_: 'start'});
    }
  }

  /*
   * update state based on new pairing code then send new location
   */
  receivePairingCode(newCode){
    this.setState({myCode_: newCode});
    this.updateLocation();
  }

  /*
   * load the main pairing screen
   */
  loadPairingScreen(){
    this.setState({screen_: 'pair'});
  }

  /*
   * render the connect sub-screen depending on the current active screen_
   */
  render() {
    switch (this.state.screen_) {
      case 'load':
        return <Loading/>
      case 'start':
        return <StartScreen callback={this.getPairingCode} goToFeed={this.props.goToFeed}/>
      case 'pending':
        return <PendingScreen userCode={this.state.myCode_}/>
      case 'success':
        return <SuccessScreen callback={this.getPairingCode} username={this.state.connectionUsername_} goToFeed={this.props.goToFeed}/>
      case 'error':
        return <ErrorScreen callback={this.loadPairingScreen}/>
      case 'timeout':
        return <TimeoutScreen callback={this.getPairingCode} goToFeed={this.props.goToFeed}/>
      case 'respond':
        return <RespondScreen callback={this.sendPairingResponse} initiatorCode={this.state.initiatorCode_}/>
      case 'pair':
        return (
          <Backdrop>
            <HeaderGoBack onPress={this.props.goToFeed} />
            <View style={styles.screen_text}>
              <Text style={styles.big_text}>{this.state.myCode_}</Text>
              <View style={{alignItems: "center"}}>
                <TextInput 
                  placeholder={"Enter friend's code"} 
                  placeholderTextColor={'#666'}
                  keyboardType={'number-pad'}
                  style={styles.num}
                  maxLength={4}
                  value={this.state.enteredCode_}
                  onChangeText={(code) => this.setState({enteredCode_: code})}
                />
              </View>
            </View>
            <TouchableOpacity
              activeOpacity={.75}
              onPress={() => this.sendPairingRequest(this.state.enteredCode_)}
              style={styles.touchable_opacity}
            >
              <Text style={styles.touchable_opacity_text}>Connect</Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={.75}
              onPress={this.updateLocation}
              style={styles.touchable_opacity}
            >
              <Text style={styles.touchable_opacity_text}>Update Location</Text>
            </TouchableOpacity>
          </Backdrop>
        );
      default:
        return <View></View>
    }
  }
}

/*
 * connection landing page
 */
const StartScreen = (props) => {
  return (
    <Backdrop>
      <HeaderGoBack onPress={props.goToFeed} />
      <View style={styles.screen_text}>
        <TouchableOpacity activeOpacity={.75} onPress={props.callback} style={styles.touchable_opacity} >
          <Text style={styles.touchable_opacity_text}>Enter Pairing Mode</Text>
        </TouchableOpacity>
      </View>
    </Backdrop>
  );
}

/*
 * error page displayed for various reaons
 */
const ErrorScreen = (props) => {
  return (
    <Backdrop>
      <HeaderGoBack onPress={props.callback} />
      <View style={styles.screen_text}>
        <Text style={styles.big_text}>Error</Text>
        <Text style={styles.small_text}>Try reentering pairing code and refreshing location</Text>
      </View>
    </Backdrop>
  );
}

/*
 * timeout page displayed when the pairing request times out
 */
const TimeoutScreen = (props) => {
  return (
    <Backdrop>
      <HeaderGoBack onPress={props.goToFeed} />
      <View style={styles.screen_text}>
        <Text style={styles.small_text}>Pairing Timed Out</Text>
        <TouchableOpacity activeOpacity={.75} onPress={props.callback} style={styles.touchable_opacity} >
          <Text style={styles.touchable_opacity_text}>Reenter Pairing Mode</Text>
        </TouchableOpacity>
      </View>
    </Backdrop>
  );
}


/*
 * pending page displayed when the pairing request has been sent
 */
const PendingScreen = (props) => {
  return (
    <Backdrop>
      <View style={styles.screen_text}>
        <Text style={styles.big_text}>Request Sent</Text>
        <Text style={styles.small_text}>Your Code: {props.userCode}</Text>
      </View>
    </Backdrop>
  );
}

/*
 * screen displayed when a new connection is successfully processed
 */
const SuccessScreen = (props) => {
  return (
    <Backdrop>
      <HeaderGoBack onPress={props.goToFeed} />
      <View style={styles.screen_text}>
        <Text style={styles.big_text}>New Connection</Text>
        <Text style={styles.small_text}>{props.username}</Text>
      </View>
      <TouchableOpacity activeOpacity={.75} onPress={props.callback} style={styles.touchable_opacity} >
        <Text style={styles.touchable_opacity_text}>Reenter Pairing Mode</Text>
      </TouchableOpacity>
    </Backdrop>
  );
}

/*
 * new connection confirmation screen
 */
const RespondScreen = (props) => {
  return (
    <Backdrop>
      <View style={styles.screen_text}>
        <Text style={styles.big_text}>New Request</Text>
        <Text style={styles.small_text}>{props.initiatorCode}</Text>
      </View>
      <TouchableOpacity activeOpacity={.75} onPress={() => props.callback('accept')} style={styles.touchable_opacity} >
        <Text style={styles.touchable_opacity_text}>Accept</Text>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={.75} onPress={() => props.callback('ignore')} style={styles.touchable_opacity} >
        <Text style={styles.touchable_opacity_text}>Ignore</Text>
      </TouchableOpacity>
    </Backdrop>
  );
}

const styles = StyleSheet.create({
  num: {
    width: 300,
    fontSize: 28,
    textAlign: 'center',
    paddingLeft: 10,
    color: '#FFEBCD',
    backgroundColor: '#333',
    alignItems: 'center',
  },
  screen_text: {
    justifyContent: 'center', 
    flex: 1, 
    paddingTop: 20,
  },
  touchable_opacity: {
    backgroundColor: "#336",
    justifyContent: "center",
    borderWidth: 2,
  },
  touchable_opacity_text: {
    color: "#FFEBCD",
    fontSize: 35,
    fontWeight: "300",
    textAlign: "center",
  },
  big_text: {
    fontWeight: '100',
    fontSize: 50,
    color: '#FFEBCD',
    textAlign: "center",
  },
  small_text: {
    fontWeight: "100",
    fontSize: 30,
    paddingTop: 15,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 30,
    color: "#FFEBCD",
    textAlign: "center",
  },
});
