import React, { Component } from "react";
import * as SecureStore from "expo-secure-store";
import Constants from "expo-constants";
import { View } from "react-native";
import Login from "./Login.js";
import Auth from "./Auth.js";
import Conn from './Conn.js';
import Feed from "./Feed.js";
import Post from "./Post.js";
import base64 from "react-native-base64";


/*
 * blank waiting screen in charge of checking whether the user has valid credentials
 * and if they do not, then user is forwarded to create a new account/credentials 
 * via the login screen.
 */
class WaitingScreen extends Component {
  constructor(props){
    super(props);
  }
  
  /*
   * at app open, we try to login using any locally saved credentials
   */
  async componentDidMount() {
    try {

      // check to see if there are any saved credentials
      const pass = await SecureStore.getItemAsync("pass");
      const num = await SecureStore.getItemAsync("num");

      // if credentials are missing, go to sign up
      if (num === null || pass === null) {
        this.props.goToLogin();
      } 
      
      // if credentials present, validate them
      else {
        this.props.validate(num, pass);
      }
    } catch (e) {
      console.log(e);
    }
  }

  /*
   * renders blank waiting screen
   */
  render() {
    return (
      <View style={{flex: 1, backgroundColor: "#444",}}>
      </View>
    );
  }
}


/*
 * the landing page
 */
export default class ShupperApp extends Component {
  constructor(props){
    super(props);
    
    this.state = {
      mode_: "load",     // determines, which view is active, defaults to load
      number_: "",       // user's cell number
      token_: undefined  // returned JWT to store u_id, used for DB queries
    }
    
    this.goToLogin = this.goToLogin.bind(this);
    this.handleValidation = this.handleValidation.bind(this);
    this.goToAuth = this.goToAuth.bind(this);  
    this.getCode = this.getCode.bind(this);
    this.verifyCode = this.verifyCode.bind(this);
    this.goToPost = this.goToPost.bind(this);
    this.goToFeed = this.goToFeed.bind(this);
    this.goToConnect = this.goToConnect.bind(this);
  }


  /* --------------- Waiting Screen --------------- */
  
  /*
   * go to the login page
   */
  goToLogin() {
    this.setState({mode_: "login"});
  }


  /*
   * log in using saved credentials
   */
  async handleValidation(num, pass) {
    try {

      // have to encode the number and pass for valid http request
      const pass_64 = base64.encode(pass);
      const num_64 = base64.encode(num);

      // attempt to login
      const res = await fetch(`https://epeake.dev/login?num=${num_64}&pass=${pass_64}`
        , {method:"POST"});
      const res_json = await res.json();

      // if credentials are good, then go to the feed and store returned token
      if (res_json.success === true) {
        this.setState({mode_: "feed", token_: res_json.token});
      } 
      
      // if there was an error with the saved credentials, then go back to login
      else {
        SecureStore.deleteItemAsync("num");
        SecureStore.deleteItemAsync("pass");
        this.setState({mode_: "login", token_: undefined});
      }
    } catch (e) {
      console.log(e);
    }
  }


  /* --------------- Login Page --------------- */
  
  /*
   * go to authorization page
   */
  goToAuth(num) {
    this.setState({mode_: "auth", number_: num});
  }


  /* --------------- Authorization Page --------------- */

  /*
   * request sends sms to client
   */
  getCode() {
    try {
      fetch(`https://epeake.dev/sign-up/${base64.encode(this.state.number_)}`
        , {method:"GET"});
    } catch (e) {
      console.log(e);
      alert("error");
    }
  }


  /*
   * verify that the user's code is valid and, if so, store the login token
   */
  async verifyCode(code) {
    try {
      const verify_res = await fetch(`https://epeake.dev/verify/${base64.encode(this.state.number_)}/${base64.encode(code)}/${base64.encode(Constants.installationId)}`
        , {method:"POST"});
      const verify_json = await verify_res.json();

      // if verified, store the num and password locally to avoid future requests
      if (verify_json.success === true) {
        SecureStore.setItemAsync("num", this.state.number_);
        SecureStore.setItemAsync("pass", Constants.installationId);
        this.setState({mode_: "feed", token_: verify_json.token});
      } else {
        alert("incorrect code");
      }
    } catch (e) {
      console.log(e);
      alert("error");
    }
  }


  /* --------------- Feed --------------- */

  /*
   * go to the posting page
   */
  goToPost() {
    this.setState({mode_: "post"});
  }


  /* --------------- Post Page --------------- */

  /*
   * go back to feed
   */
  goToFeed() {
    this.setState({mode_: "feed"});
  }

  /* --------------- Connect Page --------------- */

  /*
   * go back to connection page
   */
  goToConnect() {
    this.setState({mode_: "connect"});
  }


  /* ------------------------------ */

  /*
   * render the app's screen depending on the current mode
   */
  render() {
    switch (this.state.mode_) {

      // our blank waiting screen
      case "load":
        return <WaitingScreen validate={this.handleValidation} goToLogin={this.goToLogin}/>;
      
      // where we enter our number
      case "login":
        return <Login goToAuth={this.goToAuth}/>;

      // where we connect to other users
      case "connect":
        return <Conn token={this.state.token_} goToFeed={this.goToFeed}/>

      // where we confirm our number
      case "auth":
        return <Auth getCode={this.getCode} verify={this.verifyCode}/>;

      // where we view posts
      case "feed":
        return <Feed token={this.state.token_} goToPost={this.goToPost} goToConnect={this.goToConnect}/>;

      // where we make new posts
      case "post":
        return <Post token={this.state.token_} goToLogin={this.goToLogin} goToFeed={this.goToFeed}/>;
      
      // we should never be here
      default:
        return <View></View>;
    }
  }
}
