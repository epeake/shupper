/*
 * User login/sign-up service
 */
const express = require("express");
const Pool = require("pg").Pool
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

// twilio used for phone number validation
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const verifySid = process.env.TWILIO_VERIFY_SID;
const client = require('twilio')(accountSid, authToken);

let pool;
if (process.env.NODE_ENV !== "PROD") {
  pool = new Pool({
    host: "postgres-dev",
    port: 5432,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  });
} else {
  // our db connection object
  pool = new Pool({
    host: "postgres-prod",
    port: 5432,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  });
}


/*
 * Check that a given number is a valid cell number.
 * 
 * Returns void, but throws if number is invalid.
 */
const validateNumber = (num) => {
  // split at plus
  let plus_i = -1;
  for (let i = 1; i < 4; i++) {
    if (num[i] === '+') {
      plus_i = i;
    }
  }

  if (plus_i === -1) {
    throw "invalid number";
  }

  if (num.length - (plus_i + 1) !== 10) {
    throw "invalid number";
  }

  //check that all are numbers
  for (let i = 0; i < 10; i++) {
    if (num[i] - '0' > 9 || num[i] - '0' < 0) {
      throw "invalid number";
    }
  }
}


/*
 * Check that a given code is a valid validation code.
 * 
 * Returns void, but throws if code is invalid.
 */
const validateCode = (code) => {
  // split at plus
  if (code.length !== 6) {
    throw "invalid code";
  }

  //check that all are numbers
  for (let i = 0; i < 6; i++) {
    if (code[i] - '0' > 9 || code[i] - '0' < 0) {
      throw "invalid code";
    }
  }
}


const app = express();
app.use(bodyParser.json());


/*
 * Check to see that our server is up
 *
 * Returns:
 *  Status 200
 */
app.get("/health-check", (req, res) => {
    res.sendStatus(200);
});


if (process.env.NODE_ENV !== "PROD") {
/*
 * Troubleshooting for db. Returns all rows contained in creds.
 *
 * Returns:
 *  JSON of all rows contained in creds
 */
app.get("/test", async (req, res) => {
  try {
    const results =  await pool.query("select * from creds");
    res.status(200).json(results.rows);
  } catch (e) {
    res.status(400).json([]);
    console.log(e);
  }
});
}


/*
 * Takes number and password passed as a query arg from the req and attempts
 * to login the user. If credentials match stored credentials, then re return
 * a JWT containing the user's personally identifiable u_id.
 *
 * Returns:
 *  {
 *    success : boolean indicating whether login was successful
 *    token   : JWT of u_id if success === true
 *  }
 */
app.post("/login", async (req, res) => {
  try {
    const num = Buffer.from(req.query.num, "base64").toString("ascii");
    const pass = Buffer.from(req.query.pass, "base64").toString("ascii");

    // to prevent SQL injections.  This will throw if not valid
    validateNumber(num);

    // check to see if there is a match
    let match = { rows: []};
    match = await pool.query("select h, u_id from creds where num = $1", [num]);

    // if this is the correct password, then send a token
    if (match.rows.length !== 0 && bcrypt.compareSync(pass, match.rows[0].h)) {

      // only send over the user's id.  That is all that is needed to view posts
      jwt.sign({u_id: match.rows[0].u_id}
        , process.env.JWT_SECRET
        , { expiresIn: "24h" }
        , (err, token) => {
          if (err) { 
            console.log(err); // this shouldn't have happened
            res.status(400).json({success: false,});
          } else {
            res.status(200).json({
              success: true,
              token: token,
            });
          }
        }
      );
    }
    
    // otherwise either the number or password were incorrect
    else {
      res.status(400).json({success: false,});
    }
  } catch (e) {
    res.status(400).json({success: false,});
    console.log(e);
  }
});


/*
 * Takes number passed as a req param and, if valid, sends it twilio's
 * number verification service, which then sends an sms to the user with
 * a verification code.
 *
 * Returns:
 *  Status 200 on success else 400
 */
app.get("/sign-up/:num", async (req, res) => {
  try {
    let num = Buffer.from(req.params.num, "base64").toString("ascii");
    validateNumber(num);
    num = `+${num.replace('+', '')}`;

    // from https://www.twilio.com/docs/verify/api
    await client.verify.services(verifySid)
      .verifications
      .create({to: num, channel: "sms"});
    
    res.sendStatus(200);
  } catch (e) {
    res.sendStatus(400);
    console.log(e);
  }
});


/*
 * Verifies code sent by twilio's numer verification service coresponding 
 * to number passed as a req param. If both code and number are valid, code
 * and number are sent back to twilio, which will verify that the code is 
 * correct.
 *
 * Returns:
 *  {
 *    success : boolean indicating whether verification was successful
 *  }
 */
app.post("/verify/:num/:code", async (req, res) => {
  try {
    let num = Buffer.from(req.params.num, "base64").toString("ascii");
    validateNumber(num);
    num = `+${num.replace("+", "")}`;
    const code = Buffer.from(req.params.code, "base64").toString("ascii");
    validateCode(code);

    // from https://www.twilio.com/docs/verify/api
    const verification_check = await client.verify.services(verifySid)
      .verificationChecks
      .create({to: num, code: code});

    if (verification_check.status === 'approved') {
      res.status(200).json({success: true,});
    } else {
      res.status(400).json({success: false,});
    }
  } catch (e) {
    res.status(400).json({success: false,});
    console.log(e);
  }
});


/*
 * Creates new account for a user with number and password equal to
 * those provided as req params, permitting that the number is valid.
 * If user already exists, then we replace the user's password.
 *
 * Returns:
 *  {
 *    success : boolean indicating whether insertion was successful
 *    token   : JWT of u_id if success === true
 *  }
 */
app.put("/insert/:num/:pass", async (req, res) => {
  try {
    const num = Buffer.from(req.params.num, "base64").toString("ascii");
    validateNumber(num);

    const pass = Buffer.from(req.params.pass, "base64").toString("ascii");
    const pass_h = bcrypt.hashSync(pass, 12);

    // need to replace any existing rows then insert
    const match = await pool.query("update creds set h = $1 where num = $2", [pass_h, num]);
    
    let new_u_id;
    if (match.rowCount === 0) {
      new_u_id = await pool.query("insert into creds (num, h) values ($1, $2) returning u_id", [num, pass_h]);

      // add fake connection to test SQL queries
      if (process.env.NODE_ENV !== "PROD") {
        await pool.query("insert into connections (person_one, person_two) values (1, $1)", [new_u_id.rows[0].u_id]);
        await pool.query("insert into connections (person_one, person_two) values ($1, 1)", [new_u_id.rows[0].u_id]);
      }
    } else {
      new_u_id = await pool.query("select u_id from creds where num = $1", [num]);
    }

    // only send over the user's id.  That is all that is needed to view posts
    jwt.sign({u_id: new_u_id.rows[0].u_id}
      , process.env.JWT_SECRET
      , { expiresIn: "24h" }
      , (err, token) => {
        if (err) { 
          console.log(err); // this shouldn't have happened
          res.status(400).json({success: false,});
        } else {
          res.status(200).json({
            success: true,
            token: token,
          });
        }
      }
    );
    
  } catch (e) {
    res.status(400).json({success: false,});
    console.log(e);
  }
});


const server = app.listen(8081, () => {
  const port = server.address().port;

  console.log("Login listening on %s", port);
});
