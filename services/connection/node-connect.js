/*
 * Connection service.
 */
const http = require('http');
const express = require('express');
const WebSocket = require('ws');
const app = express();
const jwt = require("jsonwebtoken");
const Pool = require("pg").Pool

let pool;
if (process.env.NODE_ENV !== "PROD") {
  pool = new Pool({
    host: "postgres-dev",
    port: 5432,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  });
} else {
  // our db connection object
  pool = new Pool({
    host: "postgres-prod",
    port: 5432,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  });
}

//initialize a simple http server
const server = http.createServer(app);

//initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

// Key: code 
// Value: id
const codes = new Map();

// Key: id 
// Value: {client: , location: , pairingCode: , pending: {id: , confirmed: }, }
const clients = new Map();

wss.on('connection', (client) => {
  try {
    // remove items from the map when client disconnects
    client.on('close', () => {
      if(client.id){
        if(!clients.has(client.id)){
          console.log("clients entry missing for", client.id);
        }else{
          codes.delete(clients.get(client.id).pairingCode);
          clients.delete(client.id);
        }
      }
    });

    client.on('message', (message) => {
      const msg = JSON.parse(message);

      // disconnect unauthenticated connections
      if(!msg.type || (msg.type !== "auth" && !client.id)){
        client.close();
      }

      switch(msg.type) {
        case "auth":
          validateToken(client, msg);
          if(!client.id){
            client.close();
          }else{
            client.send(JSON.stringify({type: 'auth'}));
          }
          break;
        case "newCode":
          if(clients.has(client.id)){
            codes.delete(clients.get(client.id).pairingCode);
          }
          const newCode = gencode();
          clients.set(client.id, {client: client, pairingCode: newCode});
          codes.set(newCode, client.id);
          client.send(JSON.stringify({type: 'newCode', newCode: newCode}));
          break;
        case "location":
          // make sure client.id key in clients exists
          if(clients.has(client.id)){
            updateEntry(client.id, {location: msg.location})
            client.send(JSON.stringify({type: 'location'}));
          }
          break;
        case "response":
          processResponse(client, msg);

          break;
        case "request":
          const receiverId = codes.get(msg.receiverCode);
          if(!clients.has(receiverId) || !clients.has(client.id)){
            client.send(JSON.stringify({type: 'error'}));
            break;
          }
          if(!codes.has(msg.receiverCode) || msg.receiverCode === clients.get(client.id).pairingCode){
            client.send(JSON.stringify({type: 'error'}));
            break;
          }if(clients.get(client.id).pending || clients.get(receiverId).pending){
            client.send(JSON.stringify({type: 'error'}));
            break;
          }
          processRequest(client, msg);
          break;
        default:
          console.error('Bad message type :(', msg.type);
          break;
      }
    });
  } catch (error) {
    console.log(error);
  }
});


//start our server
server.listen(process.env.PORT || 8999, () => {
  console.log(`Server started on port ${server.address().port} :)`);
});


/*
 * process a pairing request
 */
function processRequest(client, msg) {
  const receiverId = codes.get(msg.receiverCode);
  const initiatorLocation = clients.get(client.id).location;
  const receiverLocation = clients.get(receiverId).location;
  let closeEnough;
  if(initiatorLocation && receiverLocation){
    closeEnough = compareLocations(initiatorLocation, receiverLocation);
  }

  if(closeEnough){
    setPending(receiverId, client.id, false);
    setPending(client.id, receiverId, true);

    clients.get(receiverId).client.send(JSON.stringify({type: 'request', initiatorCode: clients.get(client.id).pairingCode}));
    
    // maybe we should set timeouts no matter what error it was
    const receiverTimeout = setTimeout(() => {
      clients.get(receiverId).client.send(JSON.stringify({type: 'timeout'}));
    }, 1000 * 10);
    updateEntry(receiverId, {timeout: receiverTimeout});

    const initiatorTimeout = setTimeout(() => {
      client.send(JSON.stringify({type: 'timeout'}));
    }, 1000 * 10);
    updateEntry(client.id, {timeout: initiatorTimeout});

  }else{
    client.send(JSON.stringify({type: 'error'}));
  }
}

/*
 * process a pairing response
 */
async function processResponse(client, msg) {
  const initiatorId = codes.get(msg.initiatorCode);
  if(!clients.has(client.id) || !clients.has(initiatorId)){
    return;
  }
  const valid = clients.get(client.id).pending && clients.get(initiatorId).pending && clients.get(initiatorId).pending.confirmed && clients.get(client.id).pending.id === initiatorId && clients.get(initiatorId).pending.id === client.id;
  
  if(msg.response === 'accept' && valid){
    // remove pending field
    clearPending(client.id);
    clearPending(initiatorId);

    // delete deplayed timeout message
    clearTimeout(clients.get(client.id).timeout);
    clearTimeout(clients.get(initiatorId).timeout);

    // insert new connection
    await pool.query("insert into connections (person_one, person_two) values ($1, $2) on conflict do nothing", [client.id, initiatorId]);
    await pool.query("insert into connections (person_one, person_two) values ($1, $2) on conflict do nothing", [initiatorId, client.id]);
    
    // get connection username
    const initiatorUsername = await pool.query("select num from creds where u_id = $1", [initiatorId]);
    const receiverUsername = await pool.query("select num from creds where u_id = $1", [client.id]);

    client.send(JSON.stringify({type: 'success', username: initiatorUsername.rows[0].num}));
    clients.get(initiatorId).client.send(JSON.stringify({type: 'success', username: receiverUsername.rows[0].num}));
  } else if (msg.response === 'ignore'){
    clearTimeout(clients.get(client.id).timeout);
  }
}

/*
 * verify socket token for authentication
 */
function validateToken(client, msg) {
  if(msg.token){
    jwt.verify(msg.token, process.env.JWT_SECRET, (err, data) => {
      if (err) {
        client.id = undefined;
      } else {
        client.id = data.u_id;
      }
    });
  }
}

/*
 * generate a unique pairing code
 */
function gencode() {
  code = ""
  for (let i = 0; i < 4; i++) {
    code += Math.floor(Math.random() * 10).toString();
  }
  if (codes.has(code)) {
    return gencode();
  } else {
    return code;
  }
}

/*
 * update the pending field of id with connection and confirmed
 */
function setPending(id, connection, confirmed) {
  const newEntry = Object.assign(clients.get(id), {pending: {id: connection, confirmed: confirmed}});
  clients.set(id, newEntry);
}

/*
 * clear the pending field if id
 */
function clearPending(id) {
  const newEntry = Object.assign(clients.get(id), {pending: undefined});
  clients.set(id, newEntry);
}

/*
 * add data data to the value of id
 */
function updateEntry(id, data) {
  const newEntry = Object.assign(clients.get(id), data);
  clients.set(id, newEntry);
}

// http://www.movable-type.co.uk/scripts/latlong.html
/*
 * check to see if two locations are sufficiently close
 */
function compareLocations(L1, L2) {
  const lat1 = L1.coords.latitude;
  const lon1 = L1.coords.longitude;
  const lat2 = L2.coords.latitude;
  const lon2 = L2.coords.longitude;
  let R = 6371e3; // metres
  let phi1 = lat1*(Math.PI / 180);
  let phi2 = lat2*(Math.PI / 180);
  let delta_phi = (lat2-lat1)*(Math.PI / 180);
  let delta_lambda = (lon2-lon1)*(Math.PI / 180);

  let a = Math.sin(delta_phi/2) * Math.sin(delta_phi/2) +
          Math.cos(phi1) * Math.cos(phi2) *
          Math.sin(delta_lambda/2) * Math.sin(delta_lambda/2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  let d = R * c;
  if (d <= L1.coords.accuracy + L2.coords.accuracy) {
    return true;
  } else {
    return false;
  }
}
