/*
 * Posting service.
 */
const express = require("express");
const Pool = require("pg").Pool
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");

let pool;
if (process.env.NODE_ENV !== "PROD") {
  pool = new Pool({
    host: "postgres-dev",
    port: 5432,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  });
} else {
  // our db connection object
  pool = new Pool({
    host: "postgres-prod",
    port: 5432,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB
  });
}

/* 
 * Middleware for token validation.
 *
 * Appends to the request:
 *  logged_in:
 *      true: if token is valid and not expired
 *      false: otherwise
 *  u_id:
 *      u_id of the user in the database if token is valid and not expired
 * 
 * TODO: any failure should be logged as ill intent
 * 
 * Inspired by:
 * https://medium.com/dev-bits/a-guide-for-adding-jwt-token-based-authentication-to-your-single-page-nodejs-applications-c403f7cf04f4
 */
const validateToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization'];

  if (typeof token !== "undefined" && token !== "undefined") {
      if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
      }

      jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
        if (err) {
          req.logged_in = false;
        } else {
          req.logged_in = true;
          req.u_id = data.u_id;
        }
      });
  } 
  
  else {
    req.logged_in = false;
  }

  next();
}

const app = express();
app.use(bodyParser.json());


/*
 * Check to see that our server is up
 *
 * Returns:
 *  Status 200
 */
app.get("/health-check", (req, res) => {
  res.sendStatus(200);
});


if (process.env.NODE_ENV !== "PROD") {
/*
 * Troubleshooting for db. Returns all rows contained in posts.
 *
 * Returns:
 *  JSON of all rows contained in posts
 */
app.get("/test", async (req, res) => {
  try {
    const results =  await pool.query("select * from posts");
    res.status(200).json(results.rows);
  } catch (e) {
    res.status(400).json([]);
    console.log(e);
  }
});
}

/*
 * Returns all post of the user or the user's connections.
 * These rows make up the user's feed
 *
 * Returns:
 *  JSON of matching posts
 */
app.get("/feed", validateToken, async (req, res) => {
  try {
    const results =  await pool.query("select num, post, p_id from (posts inner join creds on posts.u_id = creds.u_id) where posts.u_id in (select person_two from connections where person_one = $1) or posts.u_id = $1", [req.u_id]);
    res.status(200).json(results.rows);
  } catch (e) {
    res.status(400).json([]);
    console.log(e);
  }
});


/*
 * Validates the user's credentials and inserts the post to the db
 *
 * Params: 
 *  content: the post to be inserted
 * 
 * Returns:
 *  On success if logged_in: {success: true}
 *  If not logged_in: {success: false, logged_in: false}
 *  On unanticipated failure: {success: false, logged_in: true}
 */
app.post("/post/:content", validateToken, async (req, res) => {
  try {
    if (req.logged_in) {
      const post = Buffer.from(req.params.content, "base64").toString("ascii");

      await pool.query("insert into posts (u_id, post) values ($1, $2)", [req.u_id, post]);
      
      res.status(200).json({success: true});
    } else {
      res.status(400).json({
        success: false,
        logged_in: false,
      });
    }

  } catch (e) {

    // we don't know what went wrong or if they are really logged in, but let's just leave it
    res.status(400).json({
      success: false,
      logged_in: true,
    });
    console.log(e);
  }
});

const server = app.listen(8081, () => {
  const port = server.address().port;

  console.log("Post listening on %s", port);
});
