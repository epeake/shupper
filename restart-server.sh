#!/bin/bash

COMPOSE="/usr/local/bin/docker-compose --no-ansi"
DOCKER="/usr/bin/docker"

$COMPOSE stop postgres-prod node-post node-login node-connect node-gate nginx-server
$DOCKER container prune -f
$COMPOSE up --build --no-deps postgres-prod node-post node-login node-connect node-gate nginx-server