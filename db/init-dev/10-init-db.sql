-- script run at creation of postgres container

/*
 * login tables store user credentials, it is what we use to identify users
 */
create table creds (
    u_id serial primary key,  -- personally identifiable id
    num char(14) not null,    -- cell phone number
    h char(60)                -- hashed password
); 

-- insert example accounts
insert into creds (num, h) values ('1+6109591513', '$2a$10$PnZ3PkWeDbqVM3HkVmZrXO9LbAG/pOM9KVrGNAO0bmJp/9oN.0pVW');
insert into creds (num, h) values ('1+2225550299', '$2a$14$k/7ine6FrPYdg1IapzkB1eNwBu1It1DTsn7Hw881qwOoOiokNaTUa');


/*
 * connections tables show which users are connected to which other users
 */
create table connections (
    c_id serial primary key,      -- unique connection id
    person_one integer not null,  -- person one's u_id
    person_two integer not null,  -- person two's u_id
    unique (person_one, person_two),
    foreign key (person_one) references creds(u_id) on delete cascade,
    foreign key (person_two) references creds(u_id) on delete cascade
);


/*
 * posts tables stores the posts of each user
 */
create table posts (
    p_id serial primary key,  -- unique post id
    u_id integer not null,    -- u_id of poster
    post char(250) not null,  -- post content
    foreign key (u_id) references creds(u_id) on delete cascade
);

-- insert example posts
insert into posts (u_id, post) values (1, 'really good post, yea');
insert into posts (u_id, post) values (1, 'another really good one.  This is great');
insert into posts (u_id, post) values (2, 'my first post.  wohoo');
