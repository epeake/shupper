-- script run at creation of postgres container

/*
 * login tables store user credentials, it is what we use to identify users
 */
create table creds (
    u_id serial primary key,  -- personally identifiable id
    num char(14) not null,    -- cell phone number
    h char(60)                -- hashed password
); 


/*
 * connections tables show which users are connected to which other users
 */
create table connections (
    c_id serial primary key,      -- unique connection id
    person_one integer not null,  -- person one's u_id
    person_two integer not null,  -- person two's u_id
    unique (person_one, person_two),
    foreign key (person_one) references creds(u_id) on delete cascade,
    foreign key (person_two) references creds(u_id) on delete cascade
);


/*
 * posts tables stores the posts of each user
 */
create table posts (
    p_id serial primary key,  -- unique post id
    u_id integer not null,    -- u_id of poster
    post char(250) not null,  -- post content
    foreign key (u_id) references creds(u_id) on delete cascade
);
