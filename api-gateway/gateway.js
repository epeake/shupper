/*
 * API gateway.  The forward facing interface, which forwards requests
 * to be handled by our services.
 */
const express = require('express');
const fetch = require("node-fetch");
const bodyParser = require('body-parser');

/* 
 * Middleware for token extraction.
 *
 * Appends token to the request.
 */
const getToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization'];

  if (typeof token !== "undefined" && token !== "undefined") {
    if (token.startsWith('Bearer ')) {
      token = token.slice(7, token.length);
    }
    
    req.token = token;
  } else {
    req.token = undefined;
  }

  next();
};


const app = express();
app.use(bodyParser.json());

/* 
 * This is just a placeholder
 */
app.get('/', (req, res) => {
  res.send("hello world!");
});


/* 
 * see if our servers are up and running for each of our services
 */
app.get("/health-check/", async (req, res) => {
  let all_statuses = {
    login: undefined,
    post: undefined,
    gateway: 200
  };

  try {
    const login_res = await fetch("http://node-login:8081/health-check", {method: "GET"});
    all_statuses.login = login_res.status;
  } catch (e) {
    console.log(e);
    all_statuses.login = 400;
  }

  try {
    const post_res = await fetch("http://node-post:8081/health-check", {method: "GET"});
    all_statuses.post = post_res.status;
  } catch (e) {
    console.log(e);
    all_statuses.post = 400;
  }
  
  res.status(200).json(all_statuses);
});


/* 
 * ----------node-login service requests----------
 */

/*
 * Takes number and password passed as a query arg from the req and attempts
 * to login the user. If credentials match stored credentials, then re return
 * a JWT containing the user's personally identifiable u_id.
 *
 * Returns:
 *  {
 *    success : boolean indicating whether login was successful
 *    token   : JWT of u_id if success === true
 *  }
 */
app.post('/login', async (req, res) => {
  try {
    const login_res = await fetch(
      `http://node-login:8081/login?num=${req.query.num}&pass=${req.query.pass}`
      , {method:'POST'});

    const login_json = await login_res.json();
    res.send(login_json);
  } catch (e) {
    console.log(e);
    res.status(400).json({success: false});
  }
});


/*
 * Takes number passed as a req param and, if valid, sends it twilio's
 * number verification service, which then sends an sms to the user with
 * a verification code.
 *
 * Returns:
 *  Status 200 on success else 400
 */
app.get('/sign-up/:num', async (req, res) => {
  try {

    // only call API in production because $
    if (process.env.NODE_ENV === "PROD") {
      const sign_res = await fetch(
        `http://node-login:8081/sign-up/${req.params.num}`
        , {method:'GET'});

      res.sendStatus(sign_res.status);
    } else {
      res.sendStatus(200);
    }
  } catch (e) {
    console.log(e);
    res.sendStatus(400);
  }
});


/*
 * Verifies code sent by twilio's numer verification service coresponding 
 * to number passed as a req param. If both code and number are valid, code
 * and number are sent back to twilio, which will verify that the code is 
 * correct.
 *
 * Returns:
 *  {
 *    success : boolean indicating whether verification was successful
 *  }
 */
app.post('/verify/:num/:code/:pass', async (req, res) => {
  try {
    let ver_json;
    // only call API in production because $
    if (process.env.NODE_ENV === "PROD") {
      const ver_res = await fetch(
        `http://node-login:8081/verify/${req.params.num}/${req.params.code}`
        , {method:'POST'});
      ver_json = await ver_res.json();
    } else {
      ver_json = {success: true,};
    }
    
    // if the number has been verified, we can input it in the database
    if (ver_json.success) {
      const token = await fetch(`http://node-login:8081/insert/${req.params.num}/${req.params.pass}`
        , {method:'PUT'});
      const token_json = await token.json();
      res.send(token_json);
    } 
    
    // otherwise, we indicate that there was a failure
    else {
      res.status(400).json({success: false,});
    }
    // res.send(sign_res.ver_json);
  } catch (e) {
    res.status(400).json({success: false,});
    console.log(e);
  }
});


if (process.env.NODE_ENV !== "PROD") {
  /*
   * Troubleshooting for db. Returns all rows contained in dev_login.
   *
   * Returns:
   *  JSON of all rows contained in dev_login
   */
app.get('/login/test', async (req, res) => {
  try {
    const test_res = await fetch('http://node-login:8081/test', {method:'GET'});
    const js = await test_res.json();
    res.send(js);
  } catch (e) {
    console.log(e);
    res.status(400).json([]);
  }
});
}


/* 
 * ----------node-post service requests----------
 */

/*
 * Validates the user's credentials and inserts the post to the db
 *
 * Params: 
 *  content: the post to be inserted
 * 
 * Returns:
 *  On success if logged_in: {success: true}
 *  If not logged_in: {success: false, logged_in: false}
 *  On unanticipated failure: {success: false, logged_in: true}
 */
app.post("/post/:content", getToken, async (req, res) => {
  try {
    if (req.token !== undefined) {
      const post_res = await fetch(`http://node-post:8081/post/${req.params.content}`
        , {method: "POST", headers: {"Authorization": "Bearer " + req.token,}}
      );

      const post_json = await post_res.json();
      res.send(post_json);
    } else {
      res.status(400).json({
        success: false,
        logged_in: false,
      });
    }
  } catch (e) {

    // we don't know what went wrong or if they are really logged in, but let's just leave it
    res.status(400).json({
      success: false,
      logged_in: true,
    });
    console.log(e);
  }
});


if (process.env.NODE_ENV !== "PROD") {
/*
 * Troubleshooting for db. Returns all rows contained in posts.
 *
 * Returns:
 *  JSON of all rows contained in posts
 */
app.get('/post/test', async (req, res) => {
  try {
    const test_res = await fetch('http://node-post:8081/test', {method: "GET",});
    const js = await test_res.json();
    res.send(js);
  } catch (e) {
    console.log(e);
    res.status(400).json([]);
  }
});
}


/*
 * Returns all post of the user or the user's connections.
 * These rows make up the user's feed
 *
 * Returns:
 *  JSON of matching posts
 */
app.get('/post/feed', getToken, async (req, res) => {
  try {
    const feed_res = await fetch('http://node-post:8081/feed'
      , {method: "GET", headers: {"Authorization": "Bearer " + req.token,}}
    );
    const js = await feed_res.json();
    res.send(js);
  } catch (e) {
    console.log(e);
    res.status(400).json([]);
  }
});



const server = app.listen(8081, () => {
  const port = server.address().port;

  console.log("API Gateway listening on %s", port);
});
