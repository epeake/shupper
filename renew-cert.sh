#!/bin/bash

COMPOSE="/usr/local/bin/docker-compose --no-ansi"
DOCKER="/usr/bin/docker"

cd /home/epeake/shupper/

$COMPOSE run certbot renew && $COMPOSE kill -s SIGHUP nginx-gate
$COMPOSE restart nginx-gate
$DOCKER container prune -f
$DOCKER image prune -f
